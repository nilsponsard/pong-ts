let canvas = <HTMLCanvasElement>document.getElementById("cv")
let context = <CanvasRenderingContext2D>canvas.getContext("2d")

const witdh = 800
const height = 600

canvas.height = height
canvas.width = witdh



canvas.style.background = "black"

class Ball {
    x: number
    y: number
    color: string
    size: number
    vector: Array<number>
    constructor(x: number, y: number, color: string, size: number) {
        this.x = x
        this.y = y
        this.color = color
        this.size = size
        this.vector = [1, 1]
    }
    draw(context: CanvasRenderingContext2D) {
        context.fillStyle = this.color
        context.beginPath()
        context.arc(this.x, this.y, this.size, 0, 360)
        context.fill()
        context.closePath()
    }
    update() {






        if (this.x + this.vector[0] > witdh) {
            this.vector[0] *= -1

        }
        if (this.y + this.vector[1] > height) {
            this.vector[1] *= -1

        }
        this.x += this.vector[0]
        this.y += this.vector[1]

    }
}













let ball = new Ball(100, 100, "white", 20)
let paddle1
let paddle2


function init() {

}




function loop() {
    //console.log('test')

    context.clearRect(0, 0, witdh, height)



    ball.draw(context)
    ball.update()

    requestAnimationFrame(loop)
}

loop()


